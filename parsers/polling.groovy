def getGeneral(def readerJson) {
    return readerJson.values().first()
}
def getDiscardOldBuilds(def readerJson) {
    return getGeneral(readerJson).get("discardOldBuilds")
}
def getJobParameters(def readerJson) {
    return getGeneral(readerJson).get("parameters")
}
def getJobLabel(def readerJson) {
    return getGeneral(readerJson).get("restrict_where_project_run")
}

