#!/usr/bin/env groovy

import groovy.json.JsonSlurper
import java.util.regex.*
import jenkins.model.*
import hudson.model.*

//defining path needed to processing script
workspace = "${new File(__FILE__).parent}" // __FILE__ job dsl variable
jobsDir = "${workspace}/jobs"
commonConfigurationDir = "${workspace}/configuration/common"
overridesConfigurationDir = "${workspace}/configuration/overrides"
parsersDir = "${workspace}/parsers"
branch = "${GIT_BRANCH}" // jenkins environment variable (origin/master)
branch_name = branch.tokenize('/').last() //global variable

def getBinariesJobsList() {
    def GroovyShell shell = new GroovyShell()
    def jobsReader = readJson("${jobsDir}/binaries.json")
    def jobsObject = shell.parse(new File("${parsersDir}/jobs.groovy"))
    def binariesJobsList = jobsObject.getBinariesJobs(jobsReader)
    return binariesJobsList
}

def returnLogRotator(filePath,context) {
    def typeJob = filePath.tokenize('/').last().tokenize(".").first()
    def GroovyShell shell = new GroovyShell()
    def logRotatorReader = readJson("${filePath}")
    def logRotatorObject = shell.parse(new File("${parsersDir}/${typeJob}.groovy"))
    def logRotatorList = logRotatorObject.getDiscardOldBuilds(logRotatorReader)
    for(String confOption : logRotatorList) {
        confKey = confOption.tokenize("=").first()
        confVal = confOption.tokenize("=").last()
        confVal = Eval.me(confVal)
    }
    return context."${confKey}"(*confVal)
}

def returnJobParameters(filePath,context) {
    def typeJob = filePath.tokenize('/').last().tokenize(".").first()
    def GroovyShell shell = new GroovyShell()
    def jobParametersReader = readJson("${filePath}")
    def jobParametersObject = shell.parse(new File("${parsersDir}/${typeJob}.groovy"))
    def jobParametersList = jobParametersObject.getJobParameters(jobParametersReader)
    jobParametersList.each {
        key,value ->
            confKey = key.tokenize("_").first()
            confVal = value
            confVal.each {
                it ->
                    if(it == '${branch_name}') {
                        int index = confVal.indexOf(it)
                        confVal.set( index, "${branch_name}" )
                    }
                    if(it == '${binary_repo}') {
                        int index = confVal.indexOf(it)
                        confVal.set( index, "${binary_repo}" )
                    }
            }
            return context."${confKey}"(*confVal)
    }
}

def returnJobLabel(filePath,context) {
    def typeJob = filePath.tokenize('/').last().tokenize(".").first()
    def GroovyShell shell = new GroovyShell()
    def jobLabelReader = readJson("${filePath}")
    def jobLabelObject = shell.parse(new File("${parsersDir}/${typeJob}.groovy"))
    def jobLabelList = jobLabelObject.getJobLabel(jobLabelReader)
    for(String confOption : jobLabelList) {
        confKey = confOption.tokenize("=").first()
        confVal = confOption.tokenize("=").last()
    }
    return context."${confKey}"(confVal)
}


//function to reading json file
def readJson(filePath) {
    def slurper = new JsonSlurper()
    def result = slurper.parseText(new File("${filePath}").text)
    return result
}

//funcition to create empty jobs for binaries
def createBinariesJobs() {
    for (String jobName : getBinariesJobsList()) {
        jobName = jobName.replace("\${branch_name}", "${branch_name}")
        job(jobName)
    }
}

//function to apply common configuration for jobs
def applyCommonConf() {
    for (String jobName : getBinariesJobsList()) {
        jobName = jobName.replace("\${branch_name}", "${branch_name}")
        typeJob = jobName.tokenize(".").last().toLowerCase()
        binary = jobName.tokenize("+").first()
        binary_repo = "BTS_SC_${binary}_OAM"
        job(jobName) {
            returnLogRotator("${commonConfigurationDir}/${typeJob}.json",delegate)
            returnJobLabel("${commonConfigurationDir}/${typeJob}.json",delegate)
            parameters {
                returnJobParameters("${commonConfigurationDir}/${typeJob}.json",delegate)
            }
        }
    }
}

createBinariesJobs()
applyCommonConf()
